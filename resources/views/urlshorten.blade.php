<!DOCTYPE html>
<html>
<head>

  <style type="text/css">
    
    .clippy {
    margin-top: -3px;
    position: relative;
    top: 3px;
}
img {
    border: 0;
}
  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ URL::asset('public/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('public/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ URL::asset('public/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('public/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ URL::asset('public/dist/css/skins/_all-skins.min.css') }}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ URL::asset('public/bower_components/morris.js') }}/morris.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ URL::asset('public/bower_components/jvectormap/jquery-jvectormap.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ URL::asset('public/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ URL::asset('public/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ URL::asset('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

  <!-- HTML5 Shim and Respond.js') }} IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js') }} doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="{{ URL::asset('public/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('public/https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>U</b>RL</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>URL</b>SHORTNER</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo url('/') ?>/public/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo url('/') ?>/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                
                <div class="pull-right">
                 <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout </a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
       
        <li class="active">
          <a href="{{ route('home') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            
          </a>
          
        </li>
         <li class="active">
          <a href="{{ url('urlshort') }}">
            <i class="fa fa-dashboard"></i> <span>Url Shorten</span>
            
          </a>
          
        </li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Shorten URL</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="form-horizontal" id="urlform">
              <div class="box-body">
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Title</label>

                  <div class="col-sm-10">
                    <input class="form-control" id="title" placeholder="Title" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label for="url" class="col-sm-2 control-label">URL</label>

                  <div class="col-sm-10">
                    <input class="form-control" id="url" placeholder="Enter Url(http:\\example.com)" type="text">
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button onclick="cancel();" class="btn btn-default">Cancel</button>
                <button onclick="formsubmitss();" class="btn btn-info pull-right">Shorten</button>
              </div>
              <!-- /.box-footer -->
            </div>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
         
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Shortened URL's</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
                <thead><tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th>Shorten URL</th>
                   <th>Created Date</th>
                </tr>
                 </thead>
                 <tbody id="taburl">
                
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
    </div>
  </div>
      </section>
      <!-- /.row -->
      <!-- Main row -->
      
      <!-- /.row (main row) -->

   
      <!-- /.tab-pane -->
    
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js') }} charts -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/raphael/raphael.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/bower_components/morris.js') }}/morris.min.js') }}"></script>
<!-- Sparkline -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script type="text/javascript" src="{{ URL::asset('public/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script type="text/javascript" src="{{ URL::asset('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ URL::asset('public/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ URL::asset('public/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="{{ URL::asset('public/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript" src="{{ URL::asset('public/dist/js/demo.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/dist/js/clipboard.min.js') }}"></script>

<script type="text/javascript">


  $(document).ready(function() { 


    new ClipboardJS('.btnnew');

    var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

    $.ajax({
  type: "GET",
  url: baseUrl+"/geturl",
  cache: false,
  success: function(data){
   

    var v='';
    $.each(JSON.parse(data),function(index, value){
      no=index+1;
    v+='<tr><td>'+no+'</td><td>'+value.title+'</td><td>'+value.url+'</td>'+
      '<td><span id="text'+no+'">'+value.shortenurl+'</span>'+ 
      '<button class="btnnew" data-clipboard-target="#text'+no+'"><img class="clippy" src="'+baseUrl+'/public/clippy.svg" alt="Copy to clipboard" width="13"></button></td>'+
          '<td>'+value.created_at+'</td></tr>'
    });
    
    $("#taburl").html(v);
   
  }
});
 });
  
  function CopyToClipboard(element) {

    var doc = document
    , text = doc.getElementById(element)
    , range, selection;
    
  if (doc.body.createTextRange)
    {
    range = doc.body.createTextRange();
    range.moveToElementText(text);
    range.select();
  } 
    
    else if (window.getSelection)
    {
    selection = window.getSelection();        
    range = doc.createRange();
    range.selectNodeContents(text);
    selection.removeAllRanges();
    selection.addRange(range);
  }
  document.execCommand('copy');
  window.getSelection().removeAllRanges();
  document.getElementById("btn").value="Copied";
}


function formsubmitss(){
  var title=document.getElementById("title").value;
  var url= document.getElementById("url").value
 var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
  
  $.ajax({
  type: "POST",
  url: baseUrl+"/shortenurl",
  data: {"title":title,"url":url,"_token":"{{ csrf_token() }}"},
  cache: false,
  success: function(data){
    if(data==1){
      alert("URL Not Valid")
     
  }
  else{

    var v='';
    $.each(JSON.parse(data),function(index, value){
    v+='<tr><td>'+no+'</td><td>'+value.title+'</td><td>'+value.url+'</td>'+
      '<td><span id="text'+no+'">'+value.shortenurl+'</span>'+ 
      '<button class="btnnew" data-clipboard-target="#text'+no+'"><img class="clippy" src="'+baseUrl+'/public/clippy.svg" alt="Copy to clipboard" width="13"></button></td>'+
          '<td>'+value.created_at+'</td></tr>'
    });
   
    $("#taburl").html(v);

  document.getElementById("title").value='';
  document.getElementById("url").value='';
    }
  }
});
}
function cancel(){
  document.getElementById("title").value='';
  document.getElementById("url").value='';

}


</script>

</body>
</html>
