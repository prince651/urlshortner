<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/urlshort', 'HomeController@urlshort');
Route::post('/shortenurl', 'HomeController@shortenurl');
Route::get('/redirecturl', 'HomeController@redirecturl');
Route::get('/geturl', 'HomeController@geturl');
Route::get('/getcount', 'HomeController@getcount');


