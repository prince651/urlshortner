<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Urlshortens;
use Session;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function urlshort()
    {
        return view('urlshorten');
    }
    public function shortenurl(Request $request){
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
 
        for ($i = 0; $i < 5; $i++) 
            {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
                $long_url = $request->input('url');

if (!filter_var($long_url, FILTER_VALIDATE_URL) === false)
{

$site=url('/');
$short_url = $site . "/" . $randomString;
 
$url = new Urlshortens;

$url->title = $request->input('title');
$url->url = $request->input('url');
$url->shortenurl = $short_url;

$url->save();

$short=Urlshortens::orderBy('id', 'DESC')->get();
echo json_encode($short);
 

}
else
{
$msg = 1;
echo json_encode($msg);
}



    }


    public function geturl()
    {
      $short=Urlshortens::orderBy('id', 'DESC')->get();
      echo json_encode($short);
        
    }
    public function getcount(){
        $toturl=Urlshortens::orderBy('id', 'DESC')->get()->count();
  
  $results = DB::select( DB::raw("SELECT DISTINCT(url) as title, count(url) AS count 
    FROM urlshorten 
    GROUP BY url 
    ") );
$cou=0;
  foreach ($results as $key => $value) {
    if($value->count >1){
        $cou++;
      # code...
    }
  }
  echo '[{"total":'.$toturl.' , "duplicate": '.$cou.'}]';
}
}
